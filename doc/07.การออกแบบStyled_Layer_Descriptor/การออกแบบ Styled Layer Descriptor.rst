7.	การออกแบบ Styled Layer Descriptor
=====================================
        การออกแบบการแสดงผลข้อมูลจะใช้โปรแกรม QGIS ซึ่งเป็นชอฟแวร์ที่สนับสนุนการบันทึกรูปแบบการแสดงข้อมูลแผนที่ในรูปแบบของ SLD สามารถนำมาใช้ได้กับ GeoServer และซอฟต์แวร์อื่นๆ ที่รองรับมาตรฐาน SLD สามารถออกแบบการแสดงผลให้กับข้อมูล ไม่ว่าข้อมูลนั้นจะเป็นข้อมูล Vector หรือข้อมูล Raster โดยการแสดงผลข้อมูลสามารถกำหนดให้แต่ละข้อมูลแสดงเป็นรูปแบบไหน สีอะไรได้ตามเงื่อนไขที่กำหนดให้ของแต่ละข้อมูล ข้อควรระวัง SLD  ที่มีความซับซ้อนมากเกินไปไม่สามารถ export ไปใช้งานได้ให้คำนึงถึงจุดนี้ด้วย

1. เปิดชั้นข้อมูลที่ต้องการปรับแต่งการแสดงผลในโปรแกรม QGIS 

.. figure:: img/07_1.png
    :align: center
 
2. เปิดโปรแกรม GeoServer กดปุ่ม style และเลือก Add a New Style

.. figure:: img/07_2.png
    :align: center
  
3. หน้า New Style ให้ตั้งชื่อ Style = SubDisct และวางข้อมูลที่ คัดลอกมาจาก uDig  เมื่อวางข้อมูลแล้วให้กด Submit เพื่อบันทึก style
 
 .. figure:: img/07_3.png
    :align: center

Style. ใหม่ที่ได้
 
 .. figure:: img/07_4.png
    :align: center

4. สร้าง style ให้กับข้อมูล Village และ Road
 
**7.1.	การกำหนด Style ให้กับชั้นข้อมูล**
การกำหนด style ให้กับชั้นข้อมูลต่าง ๆ ต้องเข้าไปกำหนดในหน้าของ Layer และเลือกชั้นข้อมูลแต่ละชั้นข้อมูลตาม style ที่สร้างไว้ก่อนหน้านี้

    1. กดปุ่ม Layer >> เลือกชั้นข้อมูล SubDistrict

  .. figure:: img/07_5.png
    :align: center

    2. เมื่อคลิกที่ชั้นข้อมูลจะเจอกับหน้าของ Edit Layer ให้เลือกไปที่ Publishing และเลื่อนหา Default Style หลังจากนั้นก็เลือก Style และเลือกตรงที่ Save เพื่อบันทึกการเปลี่ยนแปลง
 
 .. figure:: img/07_6.png
    :align: center

    กำหนด Default Style =SubDisct
 
  .. figure:: img/07_7.png
    :align: center
 
    3. กดปุ่ม Layer Preview แสดงชั้นข้อมูล 
ชั้นข้อมูล SubDistrict

 .. figure:: img/07_8.png
    :align: center
 
ชั้นข้อมูล Road

 .. figure:: img/07_9.png
    :align: center
 
ชั้นข้อมูล Village

 .. figure:: img/07_10.png
    :align: center
 
**7.2.	การกำหนด Style ให้กับข้อมูล Layer group**
การกำหนด Layer Groups  ต้องเข้าไปกำหนดในหน้าของ Layer Groups และเลือกกลุ่มชั้นข้อมูล จากนั้นก็จะเจอหน้า Layer Groups เลือกตรง Edit the contents of a layer groups และเลือกที่ Style และค้นหา Style  ให้กับข้อมูลแต่ละชั้นข้อมูล และเลือก Save ข้อมูล
 
  .. figure:: img/07_11.png
    :align: center

เลือก Style ให้แต่ละชั้นข้อมูล

 .. figure:: img/07_12.png
    :align: center
 
 
Layer Preview ชั้นข้อมูล layer group

 .. figure:: img/07_13.png
    :align: center

 

 
