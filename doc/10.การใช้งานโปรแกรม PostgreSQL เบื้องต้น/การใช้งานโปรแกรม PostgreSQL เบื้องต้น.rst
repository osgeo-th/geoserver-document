.. _`การใช้งานโปรแกรม PostgreSQL เบื้องต้น`:

10.	การใช้งานโปรแกรม PostgreSQL เบื้องต้น
=====================================

**PotgreSQL** 

PostgreSQL คือ ระบบจัดการฐานข้อมูลเชิงวัตถุ-สัมพันธ์ (Object-Relational DataBase Management System หรือ ORDBMS) โดยสามารถใช้รูปแบบของภาษา SQL และสามารถใช้ subselects , transactions , user-defined types และ functions ได้ และเป็น Database ซึ่งให้ Source code ฟรี 

**PostGIS**

PostGIS เป็นโปรแกรมที่ถูกพัฒนาขึ้นโดยบริษัท Refractions Research ซึ่งเป็นโครงการวิจัยทางเทคโนโลยีด้านฐานข้อมูลเชิงพื้นที่ Refractions คือ ระบบสารสนเทศภูมิศาสตร์ (GIS) และ การให้คำ ปรึกษาฐานข้อมูลของบริษัทใน Victoria, British Columbia และ Canada ซึ่งฐานข้อมูลนี้จะมีความพิเศษในการรวบรวมข้อมูล และพัฒนาซอฟท์แวร์ของลูกค้า โดยจะมีแผนการสนับสนุน และพัฒนา PostGIS เพื่อให้รองรับลำดับของความสำคัญในบทบาทหน้าที่ของระบบสารสนเทศภูมิศาสตร์ ซึ่งจะประกอบไปด้วย OpenGIS เต็มรูปแบบ ซึ่งสิ่งที่สร้างขึ้นนี้จะเป็นลักษณะภูมิประเทศในชั้นสูง (บริเวณที่ปกคลุม , พื้นผิว , เครือข่าย) ที่หน้าจอของผู้ใช้จะมีเครื่องมือที่ใช้ติดต่อสื่อสารสำหรับแสดงผลและแก้ไขข้อมูลทางสารสนเทศภูมิศาสตร์ และเครื่องมือในการเข้าถึงเว็บขั้นพื้นฐาน

.. figure:: img/10_1.png
    :align: center
 
**10.1.	การติดตั้งโปรแกรม PostgreSQL**

1. ดาวน์โหลดโปรแกรมและดับเบิ้ลคลิกที่ไฟล์ติดตั้งและกดปุ่ม Next เพื่อดำเนินการติดตั้งโปรแกรม
 
.. figure:: img/10_2.png
    :align: center

2. เลือกพื้นที่สำหรับการติดตั้งโปรแกรมและเลือก Next เพื่อดำเนินการต่อ
 
.. figure:: img/10_3.png
    :align: center
 
3. เลือกพื้นที่สำหรับการสร้างพื้นที่ Data Directory และเลือก Next
 
.. figure:: img/10_4.png
    :align: center

4. กำหนด Password สำหรับผู้ใช้งาน (ตั้ง Password = 1234567890) และเลือก Next เพื่อดำเนินการต่อ
 
.. figure:: img/10_5.png
    :align: center
 
5. กำหนด Port ให้กับฐานข้อมูล (กำหนด Port = 5432)
 
.. figure:: img/10_6.png
    :align: center

6. กำหนดค่า Lacale ของฐานข้อมูล ให้เป็น Thai, Thailand

.. figure:: img/10_7.png
    :align: center
 
7. กำหนดรายละเอียดข้างต้นสมบูรณ์แล้วโปรแกรมพร้อมสำหรับการติดตั้งโปรแกรม โดยการคลิกที่ Next เพื่อติดตั้งโปรแกรม
 
.. figure:: img/10_8.png
    :align: center

8. คลิกที่ Finish เพื่อสิ้นสุดการติดตั้งโปรแกรม

.. figure:: img/10_9.png
    :align: center 

9. เมื่อติดตั้ง PostgreSQL เสร็จแล้วให้ติดตั้งโปรแกรมเสริม (PostGIS) โดยการเลือก PostgreSQL ที่พร้อมใช้งาน (ต้องมีการเชื่อมต่อเครือข่ายอินเตอร์เน็ตขณะทำการติดตั้ง)
 
.. figure:: img/10_10.png
    :align: center

10.เลือก PostGIS 2.1 for PostgreSQL (เลือกตามระบบประฏิบัติการของคอมพิวเตอร์)
 
.. figure:: img/10_11.png
    :align: center
 
11. เลือกที่จัดเก็บโปรแกรมเสริม
 
.. figure:: img/10_12.png
    :align: center

12. เริ่มต้นการติดตั้งโปรแกรมเสริม โดยการคลิกที่ปุ่ม Next
 
.. figure:: img/10_13.png
    :align: center
 
13. กดปุ่ม I Agree เพื่อยอมรับเงื่อนไขการใช้โปรแกรม
 
.. figure:: img/10_14.png
    :align: center

14. เลือกรูปแบบการติดตั้ง เป็นแบบ Create spatial database
 
.. figure:: img/10_15.png
    :align: center

15. เลือกพื้นที่สำหรับการติดตั้งโปรแกรม
 
.. figure:: img/10_16.png
    :align: center

16. ระบุค่าการเชื่อมต่อกับฐานข้อมูล
 
.. figure:: img/10_17.png
    :align: center
 
17. ตั้งชื่อฐานข้อมูลเชิงพื้นที่ กด Install เพื่อทำการสร้างฐานข้อมูล เพื่อเตรียมไว้ใช้งานต่อไป

.. figure:: img/10_18.png
    :align: center

18. ติดตั้งเสร็จแล้วให้คลิก Close สำหรับการสิ้นสุดการติดตั้งโปรแกรม
 
.. figure:: img/10_19.png
    :align: center
 
19. การ config ให้เครื่องคอมพิวเตอร์เครื่องอื่น ๆ เข้าใช้งานฐานข้อมูลได้ เข้าไปที่โฟลเดอร์ C:\Program Files\PostgreSQL\9.4\data และเพิ่มข้อความในไฟล์ pg_hba.conf
host    all             all             192.168.1.122/32         md5
host	all	 	all	 	192.168.1.122/32	 trust
host	all		all		0.0.0.0/0		md5

.. figure:: img/10_20.png
    :align: center

 
 
**10.2.	การใช้โปรแกรม PostgreSQL เบื้องต้น**

pgAdmin เป็นโปรแกรมสำหรับเข้าไปจัดการฐานข้อมูล PostgreSQL มีลักษณะเป็น GUIs ที่ง่ายต่อการใช้งาน เมื่อติดตั้ง Postgres Plus Standard Server แล้วไม่จำเป็นต้องติดตั้ง pgAdmin เนื่องจากถูกรวมอยู่ใน Package ของ PostgreSQL อยู่แล้ว 

.. figure:: img/10_21.png
    :align: center

การเข้าใช้ระบบฐานข้อมูลเลือก PostgreSQL 9.4 (localhost:5432) โดยการ double click เพื่อเข้าสู่ฐานข้อมูล ใส่ User และ Password สำหรับการเข้าสู่ระบบ
 
.. figure:: img/10_22.png
    :align: center
 
**โปรแกรม pgAdmin III**

.. figure:: img/10_23.png
    :align: center

.. figure:: img/10_24.png
    :align: center
 
**ส่วนที่ใช้แสดงฐานข้อมูลของโปรแกรม**
 
.. figure:: img/10_25.png
    :align: center

**เครื่องมือสำหรับการ Query Tool  ไปที่ Tool >> Query Tool**
 
.. figure:: img/10_26.png
    :align: center
 
**10.3.	การนำข้อมูล Vector โดยใช้โปรแกรม PostGIS**

ฐานข้อมูลที่ไม่สามารถใช้กับข้อมูลเชิงพื้นที่ได้ ต้องลง Extensions สำหรับ Postgis ก่อน Extensions ที่จำเป็นได้แก่ postgis และpostgis_topology 

**วิธีการติดตั้ง Extensions**

1. คลิกขวาที่ Extensions >> New Extension
 
.. figure:: img/10_27.png
    :align: center

2. การติดตั้ง Extension postgis ต้องกำหนดค่าต่าง ๆ ดังนี้
Properties = postgis

.. figure:: img/10_28.png
    :align: center

Definition ต้องระบุ Schema และกดปุ่ม OK
 
.. figure:: img/10_29.png
    :align: center

3. ติดตั้ง postgis_topology

4. เมื่อติดตั้ง Extensions เสร็จ Database พร้อมที่ใช้งานกับข้อมูลเชิงพื้นที่
 
.. figure:: img/10_30.png
    :align: center

**10.4.	การนำข้อมูล Vector โดยใช้โปรแกรม PostGIS**

1. เปิดโปรแกรม PostGIS Shapefile and DBF loader 2.1
 
.. figure:: img/10_31.png
    :align: center
 
2. กดปุ่ม View connection details และกำหนดค่าการนำเข้าข้อมูล
 
.. figure:: img/10_32.png
    :align: center

เชื่อมต่อฐานข้อมูลสำเร็จ
 
 .. figure:: img/10_33.png
    :align: center

4. เมื่อเชื่อมต่อกับฐานข้อมูลได้ให้กดปุ่ม Add File

.. figure:: img/10_34.png
    :align: center
 
5. เลือกไฟล์ข้อมูล Village.shp
 
.. figure:: img/10_35.png
    :align: center

6. กำหนด Options กำหนดภาษาเป็น Windows-874 เพื่อให้โปรแกรมอ่านภาษาไทยได้
 
.. figure:: img/10_36.png
    :align: center

7. กำหนด SRID ตามระบบพิกัดของข้อมูล และกดปุ่ม Import 
 
.. figure:: img/10_37.png
    :align: center

8. นำเข้าข้อมูลสำเร็จ
 
.. figure:: img/10_38.png
    :align: center
 
9. ตรวจสอบข้อมูลที่นำเข้ามา ให้เลือกดูที่ฐานข้อมูล 
 
.. figure:: img/10_39.png
    :align: center

**แบบฝึกหัด** ให้ผู้เข้าอบรมนำเข้าข้อมูล Road และ subdistrict โดยใช้เครื่องมือ PostGIS

10. View Data คือการข้อมูลใน table 
คลิกขวาที่ชั้นข้อมูล village >> View Data >> View Top 100 Rows
 
.. figure:: img/10_40.png
    :align: center

11. ข้อมูลจากตาราง Village ที่ถูกนำมาแสดง
 
.. figure:: img/10_41.png
    :align: center
