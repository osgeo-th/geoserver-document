
.. _`การเรียกใช้ Web_Map_Service.rst`:

15. การเรียกใช้ Web_Map_Service.rst
==============================================

Web Map Service (WMS) เป็นระบบให้บริการข้อมูลภูมิสารสนเทศผ่านเครือข่าย Internet ซึ่งมีมาตรฐาน Open GIS Consortium (OGC) ที่ได้กำหนดการบริการข้อมูลภูมิสารสนเทศใน Format ต่าง ๆ ประกอบด้วย PNG, GIF หรือ JPEG ข้อมูลที่ได้มาไม่สามารถบันทึกข้อมูลเป็น Shapefile เก็บไว้

**วิธีการเชื่อมต่อ Service WMS**

1.	เลือกคำสั่ง Add WMS/WMTS Layer |logo|
2.	เลือก Layers 
3.	กดปุ่ม New กำหนดค่าดังนี้

-	Name = Training
-	URL = http://localhost:8080/geoserver/wms?
-	DPI-Mode = all

4.	กดปุ่ม OK

.. figure:: img/2.png
.. |logo| image:: img/1.png

5. กดปุ่ม Connect  และเลือกข้อมูล  กดปุ่ม Add

.. figure:: img/3.png















