6.	การเรียกใช้บริการแผนที่ Request map
==================================
         **Geospatial Consortium (OGC)** เป็นองค์กรอิสระ เกิดจากการรวมกลุ่มของบริษัทต่างๆ ในภาคอุตสาหกรรมมากกว่า 300 แห่ง ตลอดจนหน่วยงานของรัฐ ,เอกชน รวมไปถึงมหาวิทยาลัย และองค์กรอิสระต่างๆ เพื่อกำหนดกรอบมาตรฐานในทำงานร่วมกัน ก่อให้เกิดการทำงาน แบบ Interoperability สำหรับเทคโนโลยีที่เกี่ยวข้องกับสารสนเทศเชิงพื้นที่และเชิงตำแหน่ง รวมไปถึงการสนับสนุน ส่งเสริมการแบ่งปันข้อมูลปริภูมิในรูปแบบการบริการข้อมูลปริภูมิ (Bechler , 2003) มาตรฐานหลักที่เป็นที่นิยมกันใช้กันอย่างแพร่หลายได้แก่ Web Map Service (WMS) , Web Feature Service (WFS) , Web Coverage Service (WCS) , Style Layer Descriptor (SLD) , Filter Encoding (FE), Web Map Context (WMC) , Geography Markup Language (GML)

        **ประเภทของ Services ที่ควรรู้จัก**
    
        **1. WMS: Web Map Service**   จะเป็นการใช้งานร้องขอข้อมูลลักษณะของแผนที่ต่างๆ ที่มีค่าพิกัดของข้อมูลแผนที่อยู่ด้วย  Format ต่าง ๆ ประกอบด้วย PNG, GIF หรือ JPEG

        **2. WFS: Web Feature Service** จะเป็นการร้องขอข้อมูลที่เป็นข้อมูลลักษณะแผนที่ที่เป็นข้อมูล Shape file โดยผู้ใช้สามารถที่จะปรับแก้และ save ข้อมูลเป็นไฟล์ต่างๆได้

        **3. WCS: Web Coverage Service** จะเป็นการร้องขอข้อมูลที่เป็นข้อมูล Raster เช่น ข้อมูลภาพถ่ายดาวเทียมหรือข้อมูลแผนที่ที่มีลักษณะเป็นกริดภาพ
 
    **มาตรฐาน WMS: Web Map Service** มาตรฐาน การรองรับการร้องขอบริการจากผู้ใช้ โดยมีรายละเอียดใน 3 ลักษณะดังนี้ 

    **1. GetCapabilities** จะส่งค่าการให้บริการ ในส่วนของ Metadata ซึ่งเป็นตัวอธิบายเกี่ยวกับรายละเอียดของ ข้อมูลที่ให้บริการและการยอมรับค่าตัวแปรต่าง ๆ

    **2. GetMap** จะเป็นการส่งภาพแผนที่ซึ่งสามารถระบุชั้นข้อมูล ขนาดของภาพแผนที่และลักษณะของภาพ แผนที่ได้ ซึ่งรูปแผนที่แสดงภาพในรูปแบบ PNG, GIF หรือ JPEG

    **3. GetFeatureInfo** มาตรฐานตัวนี้จะเป็น Option ในการร้องขอข้อมูลเกี่ยวกับรายละเอียดของข้อมูลในแผนที่ การร้องขอการให้บริการแผนที่ ต้องระบุเงื่อนไขทางตำแหน่ง เพื่อทำการค้นคืนข้อมูลแผนที่มาใช้สำหรับการแสดง 

**อ้างอิง** : http://thaisdi.gistda.or.th 

	**ตัวอย่าง**
    
        การเรียกใช้งาน Services WMS: Reques GetCapabilities  พิมพ์ URL ดังต่อไปนี้สำหรับการสอบถามข้อมูล GetCapabilities http://localhost:8080/GeoServer/sattawat/wms?request=getCapabilities

 .. figure:: img/06_1.png
    :align: center

    **ตัวอย่าง**
    
        **การเรียกใช้งาน Services WMS: Reques GetFeatureInfo** พิมพ์ URL ดังต่อไปนี้สำหรับการสอบถามข้อมูล **GetFeatureInfo**
http://localhost:8080/geoserver/sattawat/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&FORMAT=image/png&TRANSPARENT=true&QUERY_LAYERS=layers=sattawat:subdistrict&LAYERS=layers=sattawat:subdistrict&TILED=true&INFO_FORMAT=application/json&x=50&y=50&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&STYLES=&bbox=98.2581480917421,7.47852287304961,98.4828053317799,8.20031251759669

 .. figure:: img/06_2.png
    :align: center

    **ตัวอย่าง**

        **การเรียกใช้งาน Services WMS : Request GetMap**พิมพ์ URL ดังต่อไปนั้ สำหรับการสอบถามข้อมูล **GetMap**
http://localhost:8080/geoserver/sattawat/wms?service=WMS&version=1.1.0&request=GetMap&layers=sattawat:Phuket&styles=&bbox=98.258155249389,7.47845291248579,98.4716695397294,8.20042738387495&width=330&height=768&srs=EPSG:4326&format=application/openlayers

**พารามิเตอร์สำหรับการร้องขอ WMS GetMap**

.. figure:: img/06_3.png
    :align: center
