.. _`SQL view`:

13.	SQL view
============

การกำหนดการแสดงผลข้อมูลจากชั้นข้อมูลโดยใช้คำสั่ง SQL  เช่นการแสดงผลข้อมูลเฉพาะอำเภอเมืองภูเก็ตจากชั้นข้อมูล subdistrict ที่มีข้อมูลอำเภอทั้งหมดของภูเก็ตอยู่

**วิธีการสร้าง SQL view**

1. กดปุ่ม Layer  >> Add a new resource
 
.. figure:: img/13_1.png
    :align: center

2.  กดปุ่ม Add layer from >> เลือกการเชื่อมต่อข้อมูลจาก PostGIS:PostGIS
 
.. figure:: img/13_2.png
    :align: center

3. เลือก  Configure new SQL view... 
 
.. figure:: img/13_3.png
    :align: center

4. กำหนดรายละเอียดดังนี้

**•	View Name** =ViewData 
**•	SQL statement** = Select* from "subdistrict" where "amphoe_t"like'อ.เมืองภูเก็ต'
**•**	กำหนดค่าพิกัด = 4326
**•**	กดปุ่ม Save
 
.. figure:: img/13_4.png
    :align: center
 
5. เมื่อกดปุ่ม save ก็จะเจอกับหน้า Edit Layer  ให้แก้แก้รายละเอียดดังนี้

**•**	Name = ViewData 
**•**	Title = ViewData 
**•**	Abstract = View Data Mueang Phuket

.. figure:: img/13_5.png
    :align: center

6. กำหนก Coordinate Reference Systems เลือก Declared SRS เลือกที่ Find และเลือกหาระบบพิกัด 4326
 
.. figure:: img/13_6.png
    :align: center
 
7. Bounding Boxes คลิกตรง Compute from data  และ Compute from native bounds
 
.. figure:: img/13_7.png
    :align: center

8. กำหนด Style กดปุ่ม Publishing ตั้งค่า Default Style = SubDistrict เมื่อกำหนดเสร็จแล้ว ให้ Save ชั้นข้อมูล

.. figure:: img/13_8.png
    :align: center
 
9.กดปุ่ม Layer Preview ชั้นข้อมูล View Name
 
 .. figure:: img/13_9.png
    :align: center

**แบบฝึกหัด**

1.	จงใช้ SQL view แสดงข้อมูลตำบลกะทู้จากชั้นข้อมูล subdistrict
2.	จงใช้ SQL view แสดงข้อมูลอำเภอเมืองและอำเภอกะทู้จากชั้นข้อมูล subdistrict
