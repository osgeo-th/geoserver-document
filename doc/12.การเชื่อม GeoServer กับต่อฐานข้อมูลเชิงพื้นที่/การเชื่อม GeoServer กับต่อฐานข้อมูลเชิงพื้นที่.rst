.. _`เรียนรู้ Structured Query Language (SQL) เบื้องต้นสำหรับโปรแกรม PostgreSQL`:

12.	การเชื่อม GeoServer กับต่อฐานข้อมูลเชิงพื้นที่
=======================================

**การเชื่อมต่อฐานข้อมูล PostgreSQL**

1. กดปุ่ม Stores เลือก Add new Store
 
.. figure:: img/12_1.png
    :align: center

2. เลือกการเชื่อมต่อข้อมูลแบบ PostGIS

.. figure:: img/12_2.png
    :align: center
 
3. ค่าการเชื่อมต่อกับฐานข้อมูล กำหนดรายละเอียดดังนี้ host = localhost, Port = 5432, schema = public, User = postgres และ password = 1234567890  

.. figure:: img/12_3.png
    :align: center

4.การ Publish ชั้นข้อมูลที่เชื่อมต่อมาจากฐานข้อมูล
กดปุ่ม Layer >> Add new resource >> Add layer from >> เลือก Store ที่เชื่อมต่อไว้
 
.. figure:: img/12_4.png
    :align: center

5. ข้อมูลที่เชื่อมต่อไว้ก็จะเจอกับตาราง(ชั้นข้อมูล)ที่อยู่ในดาต้าเบส ต่อจากนั้นให้เลือกชั้นข้อมูล Village และกดปุ่ม Publish
 
.. figure:: img/12_5.png
    :align: center
 
6.กำหนดรายละเอียดต่าง ๆ ให้กับข้อมูล 
 
.. figure:: img/12_6.png
    :align: center

7. กำหนด Style ให้กับชั้นข้อมูล 
 
.. figure:: img/12_7.png
    :align: center
 
8. แสดงผลข้อมูล Village ให้เลือกที่ Layer Preview และเลือกชั้นข้อมูล ตามด้วยเลือกการแสดงผลผ่าน Openlayer
 
.. figure:: img/12_8.png
    :align: center

**แบบฝึกหัด** ให้ผู้เข้าอบรม Publish ชั้นข้อมูลที่ Road และ subdistrict ที่อยู่ในฐานข้อมูล PostgreSQL
 
**ชั้นข้อมูล road**

.. figure:: img/12_9.png
    :align: center
 
**ชั้นข้อมูล subdistrict**
 
.. figure:: img/12_10.png
    :align: center
