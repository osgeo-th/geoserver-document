.. _`17.	เขียน MapboxGl  สำหรับการแสดงผลข้อมูล Web Map Service  (เบื้องต้น)`:

17.	เขียน MapboxGl  สำหรับการแสดงผลข้อมูล Web Map Service  (เบื้องต้น)
==================================================================

MapboxGl เป็น libery สำหรับการสร้าง based web mapping ในการสร้างแผนที่ MapboxGl ตัวนี้ถูกปรับปรุงให้สามารถใช้งานได้ฟรี โดยใช้ข้อมูลแผนที่ฐานจาก Thailandbasemap ที่ให้ความสนันสนุนการใช้งานโดย OSGeo Thai ซึ่ง libery ดังกล่าว สนับสนุนการใช้งานแผนที่ต่างๆ ได้แก่ WMS, WFS, Geojson เป็นต้น และสามารถเขียนคำสั่งสำหรับการใช้งานเครื่องมือได้หลากหลายสำหรับการจัดการแผนที่ ได้แก่ scale bars, overview maps, zoom bars เป็นต้น นอกจากนี้ยังสนับสนุนเครื่องมือแบบที่มีความซับซ้อนมากขึ้นโดยอาศัยคำสั่ง JavaScript มาใช้ในการเขียน

**วิธีการแสดงข้อมูล Basemap ผ่าน MapboxGl ได้ดังนี้**

1. เปิดไฟล์ index.html ด้วยโปรแกรม notepad++ จากโฟลเดอร์ osgeo-map\demo-1-call-basemap\thailand-basemap.html
2. แก้ไขข้อมูลบรรทัดที่ 5 เป็น <title>Thailand Basemap</title> เพื่อใช้สำหรับการแสดงชื่อบน Tab ข้อมูล

.. figure:: img/1.png

3. เพิ่มปุ่มควบคุมแผนที่ ซูมเข้า ซูมออก เข็มทิศ

map.addControl(new mapboxgl.NavigationControl());

4. กำหนดการแสดงผลแผนที่ฐานและตำแหน่งการซูมของแผนที่

var map = new mapboxgl.Map({
        container: 'map',
        style: JSONstyle,

        center: [100.513916,13.742053],
        zoom: 13 
    });


5. Save เพื่อบันทึกการเปลี่ยนแปลงของข้อมูล
6. เปิด Folder ที่บันทึกไฟล์ข้อมูล thailand-basemap.html คลิกขวา >> Open with >> Firefox

.. figure:: img/2.png

**วิธีการแสดงข้อมูล Web map Service (wms) ผ่าน MapboxGl ได้ดังนี้**

1. เปิดไฟล์ index.html ด้วยโปรแกรม notepad++ จากโฟลเดอร์ osgeo-map\demo-2-call-wms\thailand-basemap.html
2. แก้ไข  Code ตามนี้ เพื่อใช้สำหรับการแสดงข้อมูล wms ที่ได้จาก Service ของ GeoServer

>>> Script

map.on('load', function () {
            map.addSource('wms-test-source', {
                'type': 'raster',
                'tiles': [
                    'http://url/geoserver/wms?&bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=layername'
                ],
                'tileSize': 256
            });
            map.addLayer(
                {
                    'id': 'wms-test-layer',
                    'type': 'raster',
                    'source': 'wms-test-source',
                    'paint': {}
                }
            );
        });

>>> Script

map.addSource('points', {
        type: 'geojson',
        data: 'http://url/geoserver/workspace/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=store:layer&outputFormat=application%2Fjson'
        });
        map.addLayer(
                {
                "id": "points",
                "type": "fill",
                "source": "points",
                "paint": {"fill-color": "hsl(205, 56%, 73%)", "fill-opacity": 0}
                    }
                );
                map.on('click', 'points', function(e) {
            new mapboxgl.Popup()
                .setLngLat(e.lngLat)
                .setHTML('<b>ตำบล : </b> ' +e.features[0].properties.TambonTH + '<br/>' +'<b>อำเภอ : </b>' +e.features[0].properties.AmphoeTH + '<br/>' +'<b>จังหวัด : </b>' +e.features[0].properties.ProvinceTH)
                .addTo(map);
        });
 
        // Change the cursor to a pointer when the mouse is over the states layer.
        map.on('mouseenter', 'points', function() {
            map.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map.on('mouseleave', 'points', function() {
            map.getCanvas().style.cursor = '';
        });



3. เปิด Folder ที่บันทึกไฟล์ข้อมูล thailand-basemap.html คลิกขวา >> Open with >> Firefox

.. figure:: img/3.png

**วิธีการแสดงข้อมูล Information ข้อมูลแผนที่ ผ่าน MapboxGl ได้ดังนี้**

1. เปิดไฟล์ index.html ด้วยโปรแกรม notepad++ จากโฟลเดอร์ osgeo-map\demo-3-call-information\thailand-basemap.html
2. แก้ไข  Code ตามนี้ เพื่อใช้สำหรับการแสดงข้อมูล information ที่ได้จาก Service ของ GeoServer

>>> Script

map.addSource('points', {
        type: 'geojson',
        data: 'http://url/geoserver/workspace/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=store:layer&outputFormat=application%2Fjson'
        });
        map.addLayer(
                {
                "id": "points",
                "type": "fill",
                "source": "points",
                "paint": {"fill-color": "hsl(205, 56%, 73%)", "fill-opacity": 0}
                    }
                );
                map.on('click', 'points', function(e) {
            new mapboxgl.Popup()
                .setLngLat(e.lngLat)
                .setHTML('<b>ตำบล : </b> ' +e.features[0].properties.TambonTH + '<br/>' +'<b>อำเภอ : </b>' +e.features[0].properties.AmphoeTH + '<br/>' +'<b>จังหวัด : </b>' +e.features[0].properties.ProvinceTH)
                .addTo(map);
        });
 
        // Change the cursor to a pointer when the mouse is over the states layer.
        map.on('mouseenter', 'points', function() {
            map.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map.on('mouseleave', 'points', function() {
            map.getCanvas().style.cursor = '';
        });



.. figure:: img/code5.PNG

3. เปิด Folder ที่บันทึกไฟล์ข้อมูล thailand-basemap.html คลิกขวา >> Open with >> Firefox

.. figure:: img/4.png

**วิธีการแสดงข้อมูล legendgraphic ข้อมูลแผนที่ ผ่าน MapboxGl ได้ดังนี้**

1. เปิดไฟล์ index.html ด้วยโปรแกรม notepad++ จากโฟลเดอร์ osgeo-map\demo-4-legendgraphic\thailand-basemap.html
2. แก้ไข  Code ตามนี้ เพื่อใช้สำหรับการแสดงข้อมูล legendgraphic ที่ได้จาก Service ของ GeoServer

.. figure:: img/5.png

>>> css

.legend {
            background-color: #fff;
            border-radius: 3px;
            bottom: 50px;
            /* box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1); */
            font: 4px/5px 'Open Sans', sans-serif;
            padding: 10px;
            position: absolute;
            right: 60px;
            z-index: 1;
        }

        .legend h4 {
            margin: 0 0 10px;
        }

        .legend div span {
            border-radius: 50%;
            display: inline-block;
            height: 10px;
            margin-right: 10px;
            width: 10px;
        }


>>> script

<div id="legend" class="legend">
        <h4 style="font-size:12px"> คำอธิบานสัญลักษณ์ <br><br><img
                src="http://url/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=30&LAYER=layers"
                class="w3-circlew3-margin-right" style="width:60px"></a></h4>
    </div>


3. เปิด Folder ที่บันทึกไฟล์ข้อมูล thailand-basemap.html คลิกขวา >> Open with >> Firefox

.. figure:: img/6.png


