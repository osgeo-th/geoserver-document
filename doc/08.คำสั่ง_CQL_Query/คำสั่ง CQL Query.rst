8.	คำสั่ง CQL Query
===================
        Common Query Language Query (CQL Query) หมายถึงการแสดงข้อมูลเฉพาะส่วนที่ต้องการให้แสดง โดยนำค่ามาจากตาราง Attribute ของชั้นข้อมูล Vector การแสดงผลจะต้องระบุเงื่อนไขสำหรับการค้นหาข้อมูลที่มีอยู่ในตาราง Attribute มาแสดง เช่น ถ้าต้องการให้แสดงเฉพาะอำเภอเมืองภูเก็ต จากคอลัมน์ NAME ใช้คำสั่ง “NAME "='อ.เมืองภูเก็ต'  

    **การใช้งาน CQL Query**
    1. กดปุ่ม Layer Preview >> เลือกชั้นข้อมูล SubDistrict >> Openlayers
 
.. figure:: img/08_1.png
    :align: center

    2. ชั้นข้อมูล SubDistrict ที่แสดงผลผ่าน Openlayer   
 
.. figure:: img/08_2.png
    :align: center

    3. การเรียกใช้งาน Filter CQL กดที่ปุ่ม  
    ถ้าต้องการแสดงผลข้อมูลเฉพาะอำเภอเมือง  คำสั่งที่ใช้คือ  "AMPHOE_T"='อ.เมืองภูเก็ต'

.. figure:: img/08_3.png
    :align: center

    ผลลัพธ์การค้นหาข้อมูล โดยใช้ Filter CQL

.. figure:: img/08_4.png
    :align: center
 
    4. ถ้าต้องการยกเลิกการ Filter CQL ให้เลือกที่เครื่องหมายกากบาท   
    ผลลัพธ์ที่ได้ การยกเลิก Filter CQL
 
.. figure:: img/08_5.png
    :align: center
 
แบบฝึกหัด

    1.	จงให้คำสั่ง CQL  ค้นหาอำเภอที่มี ‘ก’ จากชั้นข้อมูล subdistrict

.. figure:: img/08_6.png
    :align: center
 
    2. จงให้คำสั่ง CQL  ค้นหาตำบลที่มี ‘ก’ จากชั้นข้อมูล subdistrict

.. figure:: img/08_7.png
    :align: center
