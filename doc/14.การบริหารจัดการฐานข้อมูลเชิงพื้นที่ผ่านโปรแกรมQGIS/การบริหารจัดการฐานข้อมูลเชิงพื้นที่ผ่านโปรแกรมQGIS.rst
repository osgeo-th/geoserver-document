
.. _`การบริหารจัดการฐานข้อมูลเชิงพื้นที่ผ่านโปรแกรม QGIS`:

14. การบริหารจัดการฐานข้อมูลเชิงพื้นที่ผ่านโปรแกรม QGIS
==============================================

ฐานข้อมูลเชิงพื้นที่ PostgreSQL สามารถนำข้อมูลจากฐานข้อมูลเชิงพื้นที่มาแก้ไข และอัพเดทข้อมูลต่างๆ ในตาราง Attribute ได้ ทำให้ผู้ใช้งานไม่เสียเวลาในการอัพเดทชั้นข้อมูลและเข้าไปในฐานข้อมูลเชิงพื้นที่อีกครั้ง และที่สำคัญข้อมูลที่ได้จะมีความทันสมัยอยู่ตลอดเวลา

**14.1 วิธีการเชื่อมต่อกับฐานข้อมูล PostgreSQL**

1. กดปุ่มคำสั่ง Add PostGIS Table |logo|
2. กดปุ่ม New Connection สำหรับการสร้าง Connection เชื่อมต่อกับข้อมูล
3. กำหนดค่าต่างๆ สำหรับการเชื่อมต่อข้อมูล โดยการรายละเอียดต่างๆ ดังนี้

•	Name = postgis
•	Host = localhost
•	Port = 8080
•	Database = Postgis
•	Username = postgres
•	Password = 1234567890
•	ทำเครื่องหมาย x ตรง Save Username และ Password
•	กดปุ่ม Test Connect 

.. figure:: img/img2.png
.. |logo| image:: img/img1.png

4. กดชื่อที่เชื่อมต่อไว้สำหรับการ Connect เข้าสู่ Database

.. figure:: img/img3.png

5. ชั้นข้อมูลต่าง ๆ จากฐานข้อมูลเชิงพื้นที่ PostgreSQL

.. figure:: img/img4.png

6. เลือกชั้นข้อมูล subdistrict >> กดปุ่ม Add Select layer to canvas

.. figure:: img/img5.png

7. การแก้ไขและอัพเดทข้อมูลต่างๆ ต้องแก้ในตาราง Attribute โดยใช้วิธีการแก้ไขแบบเดียวกับการนำเข้าข้อมูลแบบ Shapefile

.. figure:: img/img6.png 

8. การบันทึกข้อมูลจากฐานข้อมูล PostgreSQL เป็นข้อมูล Shapefile 

คลิกขวาที่ชั้นข้อมูล >> Properties >> Save As >> Add saved file to map >> เลือกที่เก็บข้อมูล >> Save >> OK

.. figure:: img/img7.png

**14.2 การจัดการข้อมูลโดยใช้โปรแกรม QGIS**

โปรแกรม QGIS มีความสามารถสำหรับจัดการข้อมูลฐานข้อมูล PostgreSQL โดยใช้เครื่องมือ Databese Manager เครื่องมือดังกล่าวสามารถแก้ไข้, เพิ่มและลบข้อมูลได้ รวมถึงการสร้างตารางใหม่สำหรับชั้นข้อมูลใหม่ โดยไม่ต้องใช้คำสั่ง SQL 

.. figure:: img/img8.png

**วิธีการจัดการข้อมูลสามารถทำได้ดังนี้**

1. เชื่อมต่อกับฐานข้อมูล PostgreSQL
2. กดปุ่ม Databese >> DB Manager >> DB Manager

.. figure:: img/img9.png

3. การตรวจสอบ Information ข้อมูล ให้เลือกที่ชื่อตารางและกดปุ่ม Info

.. figure:: img/img10.png

4. ดูข้อมูลในตาราง (Attribute) ให้กดปุ่ม Table

.. figure:: img/img11.png

5. การดูตัวอย่างแผนที่ กดปุ่ม Preview

.. figure:: img/img12.png

6. การนำข้อมูลเข้าฐานข้อมูล กดปุ่ม import layer/file  |logo1|

.. |logo1| image:: img/img13.png

**ตั้งค่าต่าง ๆ ดังนี้**

•	Imput คือชั้นข้อมูลที่ต้องการนำเข้าสู่ฐานข้อมูลให้เลือกชั้นข้อมูล MeangPhuket
•	Schema คือพื้นที่ที่ต้องการจะกัดเก็บชั้นข้อมูล
•	Table คือ ชื่อตารางให้ตั้งชื่อ MeangPhuket
•	Soure SRID คือระบบพิกัดของข้อมูล
•	กดปุ่ม OK

.. figure:: img/img14.png

ชั้นข้อมูลที่อยู่ในฐานข้อมูล

.. figure:: img/img15.png

7. การนำข้อมูลออกจากฐานข้อมูล กดปุ่ม Export to file |logo2|

.. |logo2| image:: img/img16.png

**ตั้งค่าต่าง ๆ  ดังนี้**

•	Output เลือกที่เก็บข้อมูลไปที่ Vector/Output ตั้งชื่อไฟล์ SubDis
•	Options ตั้งค่าตามพื้นฐานตามโปรแกรมกำหนด
•	กดปุ่ม OK

.. figure:: img/img17.png

8. การจัดการข้อมูลตาราง กดปุ่ม Table

•	Create table คือการสร้างตารางใหม่
•	Edit table คือการแก้ไขข้อมูลตาราง
•	Dellet คือกการลบตาราง 
•	Empty table คือการลบข้อมูลในตาราง 
•	Move to schema คือการย้าย schema

8.1 Create table

**ตั้งค่าต่าง ๆ ดังนี้**

•	Schema เลือก public
•	Name ตั้งชื่อ Point
•	Add field 
•	กำหนด Primary key ที่คอลัมน์ ID
•	Create geoetry column กำหนดเป็น point ตามประเภทข้อมูลที่ต้องการสร้าง
•	ตั้งชื่อคอลัมน์ geometry = geom
•	กำหนด SRID = 4326
•	ทำเครื่องหมายเลือก Create spatial index

.. figure:: img/img18.png

ข้อมูลตารางที่อยู่ในฐานข้อมูล

.. figure:: img/img19.png

8.2 การสร้างข้อมูล

- เลือกที่ชั้นข้อมูล คลิกขวาเลือก Add to cavas 
- สามารถสร้างข้อมูลได้แบบเดี่ยวกับสร้างข้อมูล Shapefile

.. figure:: img/img20.png

ข้อมูลที่แสดงบนโปรแกรม QGIS

.. figure:: img/img21.png

ข้อมูลในตาราง Attribute

.. figure:: img/img22.png

ข้อมูลที่แสดงในฐานข้อมูล PostgreSQL

.. figure:: img/img23.png









