.. _`เรียนรู้ Structured Query Language (SQL) เบื้องต้นสำหรับโปรแกรม PostgreSQL`:

11.	เรียนรู้ Structured Query Language (SQL) เบื้องต้นสำหรับโปรแกรม PostgreSQL
=======================================================================

การค้นหาข้อมูลที่ต้องการ โดยการกำหนดเงื่อนไขเพื่อใช้ในการค้นหาข้อมูลที่มีอยู่มาแสดง ซึ่งการจะแสดงในรูปแบบของตารางข้อมูล  การสืบค้นข้อมูลในโปรแกรม PostgreSQL ใช้คำสั่ง Query Tool

**วิธีการใช้คำสั่ง Query Tool**

1. คำสั่งให้แสดงข้อมูลทั้งหมดจากตาราง village 
คำสั่งที่ใช้ select* from village
 
.. figure:: img/11_1.png
    :align: center

2. คำสั่งให้แสดงข้อมูลจากตาราง village คอลัมน์ objecid = 1
คำสั่งที่ใช้ select* from village where "objectid" = 1
 
.. figure:: img/11_2.png
    :align: center

3. คำสั่งให้แสดงข้อมูลจากตาราง village คอลัมน์ objecid = 1 และ 2
คำสั่งที่ใช้ select* from village where "objectid" = 1 OR "objectid" = 2
 
 .. figure:: img/11_3.png
    :align: center

4. คำสั่งให้แสดงข้อมูลจากตาราง village คอลัมน์ vil_name_t"  = บ้านสวนมะพร้าว (1)
คำสั่งที่ใช้ select* from village where "vil_name_t" = 'บ้านสวนมะพร้าว (1)'
 
.. figure:: img/11_4.png
    :align: center

**แบบฝึกหัด**

1. จงใช้ SQL ค้าหาข้อมูลอำเภอเมืองจากชั้นข้อมูล subdistrict
2. จงใช้ SQL ค้าหาข้อมูลอำเภอเมืองและอำเภอกะทู้จากชั้นข้อมูล subdistrict
