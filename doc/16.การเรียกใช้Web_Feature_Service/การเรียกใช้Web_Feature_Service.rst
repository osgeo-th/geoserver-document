.. _`การเรียกใช้ Web Feature Service`:

16. การเรียกใช้ Web Feature Service
===================================

การเรียกใช้งาน Web Feature Service (WFS) เป็นการให้บริการข้อมูลแผนที่ที่เป็นแบบ vector ที่อยู่ในรูปแบบของ CSV,GML,GeoJSON หรือ Shapefile ก็ได้ ผู้ใช้งานสามารถบันทึกและแปลงข้อมูลให้อยู่ในรูปแบบที่พร้อมนำไปวิเคราะห์หรือใช้งานต่อได้

**วิธีการเชื่อมต่อ Service WMS**

1.	เลือกคำสั่ง Add WMS/WMTS Layer  |logo|
2.	กดปุ่ม New กำหนดค่าดังนี้

-	Name = Training
-	URL = http://localhost:80820/geoserver/wfs?

3.	กดปุ่ม OK

.. figure:: img/2.png
.. |logo| image:: img/1.png

4.	กดปุ่ม Connect  และเลือกข้อมูล  กดปุ่ม Add

.. figure:: img/3.png

**Note** : การบันทึกข้อมูลเป็น Shapefile คลิกขวาที่ชั้นข้อมูล >> Properties >> Save  As >> Add saved file to map >>  เลือกที่เก็บ >> Save >> OK