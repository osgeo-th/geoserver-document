<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="0" simplifyAlgorithm="0" simplifyLocal="1" readOnly="0" maxScale="0" simplifyDrawingTol="1" version="3.0.0-Girona" simplifyMaxScale="1" minScale="1e+8" simplifyDrawingHints="0" hasScaleBasedVisibilityFlag="0">
  <renderer-v2 type="graduatedSymbol" enableorderby="0" graduatedMethod="GraduatedSize" forceraster="0" attr="Transactio" symbollevels="0">
    <ranges>
      <range symbol="0" lower="18.000000000000000" render="true" label=" 18.0000 - 69.6000 " upper="69.599999999999994"/>
      <range symbol="1" lower="69.599999999999994" render="true" label=" 69.6000 - 121.2000 " upper="121.199999999999989"/>
      <range symbol="2" lower="121.199999999999989" render="true" label=" 121.2000 - 172.8000 " upper="172.799999999999983"/>
      <range symbol="3" lower="172.799999999999983" render="true" label=" 172.8000 - 224.4000 " upper="224.399999999999977"/>
      <range symbol="4" lower="224.399999999999977" render="true" label=" 224.4000 - 276.0000 " upper="276.000000000000000"/>
    </ranges>
    <symbols>
      <symbol clip_to_extent="1" type="marker" alpha="1" name="0">
        <layer enabled="1" locked="0" pass="0" class="SvgMarker">
          <prop k="angle" v="0"/>
          <prop k="color" v="136,210,210,255"/>
          <prop k="fixedAspectRatio" v="0"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="name" v="D:/Lab/SVG/KTB.svg"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="1"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" type="marker" alpha="1" name="1">
        <layer enabled="1" locked="0" pass="0" class="SvgMarker">
          <prop k="angle" v="0"/>
          <prop k="color" v="136,210,210,255"/>
          <prop k="fixedAspectRatio" v="0"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="name" v="D:/Lab/SVG/KTB.svg"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2.75"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" type="marker" alpha="1" name="2">
        <layer enabled="1" locked="0" pass="0" class="SvgMarker">
          <prop k="angle" v="0"/>
          <prop k="color" v="136,210,210,255"/>
          <prop k="fixedAspectRatio" v="0"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="name" v="D:/Lab/SVG/KTB.svg"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="4.5"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" type="marker" alpha="1" name="3">
        <layer enabled="1" locked="0" pass="0" class="SvgMarker">
          <prop k="angle" v="0"/>
          <prop k="color" v="136,210,210,255"/>
          <prop k="fixedAspectRatio" v="0"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="name" v="D:/Lab/SVG/KTB.svg"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="6.25"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" type="marker" alpha="1" name="4">
        <layer enabled="1" locked="0" pass="0" class="SvgMarker">
          <prop k="angle" v="0"/>
          <prop k="color" v="136,210,210,255"/>
          <prop k="fixedAspectRatio" v="0"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="name" v="D:/Lab/SVG/KTB.svg"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="8"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol clip_to_extent="1" type="marker" alpha="1" name="0">
        <layer enabled="1" locked="0" pass="0" class="SvgMarker">
          <prop k="angle" v="0"/>
          <prop k="color" v="136,210,210,255"/>
          <prop k="fixedAspectRatio" v="0"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="name" v="D:/Lab/SVG/KTB.svg"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="4"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <mode name="equal"/>
    <rotation/>
    <sizescale/>
    <labelformat decimalplaces="0" trimtrailingzeroes="false" format=" %1 - %2 "/>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>"Name"</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory scaleBasedVisibility="0" width="15" enabled="0" maxScaleDenominator="1e+8" minScaleDenominator="0" labelPlacementMethod="XHeight" penAlpha="255" scaleDependency="Area" height="15" penWidth="0" backgroundAlpha="255" diagramOrientation="Up" backgroundColor="#ffffff" penColor="#000000" sizeType="MM" minimumSize="0" opacity="1" barWidth="5" rotationOffset="270" lineSizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8,-1,5,50,0,0,0,0,0"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" dist="0" showAll="1" placement="0" linePlacementFlags="18" priority="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <fieldConfiguration>
    <field name="No.">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Latitude">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Longitude">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Profit or">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Transactio">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="GRADE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="No." index="0" name=""/>
    <alias field="Name" index="1" name=""/>
    <alias field="Latitude" index="2" name=""/>
    <alias field="Longitude" index="3" name=""/>
    <alias field="Profit or" index="4" name=""/>
    <alias field="Transactio" index="5" name=""/>
    <alias field="GRADE" index="6" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="No." expression="" applyOnUpdate="0"/>
    <default field="Name" expression="" applyOnUpdate="0"/>
    <default field="Latitude" expression="" applyOnUpdate="0"/>
    <default field="Longitude" expression="" applyOnUpdate="0"/>
    <default field="Profit or" expression="" applyOnUpdate="0"/>
    <default field="Transactio" expression="" applyOnUpdate="0"/>
    <default field="GRADE" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="0" field="No." exp_strength="0" unique_strength="0" constraints="0"/>
    <constraint notnull_strength="0" field="Name" exp_strength="0" unique_strength="0" constraints="0"/>
    <constraint notnull_strength="0" field="Latitude" exp_strength="0" unique_strength="0" constraints="0"/>
    <constraint notnull_strength="0" field="Longitude" exp_strength="0" unique_strength="0" constraints="0"/>
    <constraint notnull_strength="0" field="Profit or" exp_strength="0" unique_strength="0" constraints="0"/>
    <constraint notnull_strength="0" field="Transactio" exp_strength="0" unique_strength="0" constraints="0"/>
    <constraint notnull_strength="0" field="GRADE" exp_strength="0" unique_strength="0" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="No."/>
    <constraint exp="" desc="" field="Name"/>
    <constraint exp="" desc="" field="Latitude"/>
    <constraint exp="" desc="" field="Longitude"/>
    <constraint exp="" desc="" field="Profit or"/>
    <constraint exp="" desc="" field="Transactio"/>
    <constraint exp="" desc="" field="GRADE"/>
  </constraintExpressions>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" width="-1" hidden="0" name="No."/>
      <column type="field" width="-1" hidden="0" name="Name"/>
      <column type="field" width="-1" hidden="0" name="Latitude"/>
      <column type="field" width="-1" hidden="0" name="Longitude"/>
      <column type="field" width="-1" hidden="0" name="Profit or"/>
      <column type="field" width="-1" hidden="0" name="Transactio"/>
      <column type="field" width="-1" hidden="0" name="GRADE"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <editform></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="GRADE"/>
    <field editable="1" name="Latitude"/>
    <field editable="1" name="Longitude"/>
    <field editable="1" name="Name"/>
    <field editable="1" name="No."/>
    <field editable="1" name="Profit or"/>
    <field editable="1" name="Transactio"/>
  </editable>
  <labelOnTop>
    <field name="GRADE" labelOnTop="0"/>
    <field name="Latitude" labelOnTop="0"/>
    <field name="Longitude" labelOnTop="0"/>
    <field name="Name" labelOnTop="0"/>
    <field name="No." labelOnTop="0"/>
    <field name="Profit or" labelOnTop="0"/>
    <field name="Transactio" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <expressionfields/>
  <previewExpression>Name</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
